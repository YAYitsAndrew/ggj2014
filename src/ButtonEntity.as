package
{
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Input;
	
	public class ButtonEntity extends InteractableEntity implements IEventDispatcher
	{
		private var _enabled : Boolean;
		
		public function ButtonEntity( a_text : String, a_width : int = 120, a_height : int = 60 )
		{
			super( {
				id:"TitleButton",
				frameWidth:120,
				frameHeight:60,
				sheetName:Assets.TITLE_BUTTON,
				dryFrames:[0],
				wetFrames:[1]
			} ); //lol
			
			dispatcher = new EventDispatcher( this );
			
			var txt : Text = new Text( a_text );
			txt.color = 0x000000;
			this.addGraphic( txt );
			txt.x = _img.x + (  _img.width - txt.width ) / 2;
			txt.y = _img.y + ( _img.height - txt.height ) / 2;
			
			_enabled = true;
		}
		
		override public function update() : void
		{
			super.update();
			
			if ( _enabled && Input.mousePressed && this.collidePoint( x, y, Input.mouseX, Input.mouseY ) )
			{
				dispatcher.dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
			}
		}
		
		override protected function onFullyWet():void
		{
			super.onFullyWet();
			
			FP.world = new MainWorld();
		}
		
		public function get enabled() : Boolean { return _enabled; }
		public function set enabled( a_val : Boolean ) : void { _enabled = a_val; }
		
		/***************************
		 * IEVENTDISPATCHER NONSENSE
		 **/
		private var dispatcher : EventDispatcher;
		
		public function addEventListener( type : String, listener : Function, useCapture : Boolean = false,
			priority : int = 0, useWeakReference : Boolean = false ) : void
		{
			dispatcher.addEventListener( type, listener, useCapture, priority );
		}
		
		public function dispatchEvent( evt : Event ) : Boolean
		{
			return dispatcher.dispatchEvent( evt );
		}
		
		public function hasEventListener( type : String ) : Boolean
		{
			return dispatcher.hasEventListener( type );
		}
		
		public function removeEventListener( type : String, listener : Function, useCapture : Boolean = false ) : void
		{
			dispatcher.removeEventListener( type, listener, useCapture );
		}
		
		public function willTrigger( type : String ) : Boolean
		{
			return dispatcher.willTrigger( type );
		}
	}

}