package
{

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Emitter;
import net.flashpunk.graphics.Graphiclist;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Ease;
import net.flashpunk.utils.Input;
import net.flashpunk.graphics.Spritemap;

public class SpeechBubble extends Entity
{	
	private var _textImg : Text;
	private var _textBacking : Text;
	private var _renderableCollection : Graphiclist;
	
	public function SpeechBubble(speechString:String)
	{
		_textImg = new Text( speechString );
		_textImg.x = _textImg.width * -0.5;
		_textImg.y = _textImg.height * -0.5;
		
		_textBacking = new Text(speechString);
		_textBacking.color = 0x000000;
		_textBacking.x = _textImg.width * -0.5;
		_textBacking.y = _textImg.height * -0.5 + 1;
		
		_renderableCollection = new Graphiclist();
		
		_renderableCollection.add(_textBacking);
		_renderableCollection.add(_textImg);
		
		super( 0, 0, _renderableCollection );
	}
}

}