package
{

import flash.geom.Rectangle;
import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Backdrop;
import net.flashpunk.graphics.Canvas;
import net.flashpunk.graphics.Text;
import net.flashpunk.tweens.misc.Alarm;
import net.flashpunk.World;
import net.flashpunk.utils.Input;

public class MainWorld extends World
{
	public static const SCROLL_BUFFER : int = 100;
	public static const SCROLL_RATE : int = 300; //pixels/sec
	public static const ENTITY_OFFSET_X : int = 0;
	public static const MINIMUM_PLACEMENT_DISTANCE : int = 200;
	public static const VARIABLE_PLACEMENT_DISTANCE : int = 200;
	
	private static const WET_MESSAGES : Array = [
		"What a good little raincloud you are. Do one more.",
		"This is fun, how about another?",
		"Don't just pick anything, what do you want to wet?",
		"Only enough water for one more, choose carefully.",
		"Alas, you are without rain."
	];
	
	private var _cloud : CloudEntity;
	private var _bgEntity : Entity;
	private var _interactableCollection:Array;
	private var _rightEdgeOfWorld : Number;
	private var _worldText : Text;
	private var _textBack : Canvas;
	private var _worldTextAlarm : Alarm;
	private var _moreText : Text;
	private var _reportedWetCount : int;
	
	public function MainWorld()
	{
		super();
		
		_reportedWetCount = 0;
	}
	
	override public function begin() : void
	{
		super.begin();
		
		createScene();
		createAvatar();
		createInteractableEntities();
		createText();
		
		Player.getInstance().initialize();
		
		storyPart1();
	}
	
	private function storyPart1() : void
	{
		this.camera.x = FP.screen.width * 2;
		_cloud.x = FP.screen.width * 2.5; //middle of 3rd screen
		_cloud.listenToMouse = false;
		
		showWorldText( "We begin on a sunny day in the park...", 5.8 );
		FP.tween( this.camera, { x : FP.screen.width }, 6, { complete : storyPart2 } );
		FP.tween( _cloud, { x : FP.screen.width * 1.5 }, 6 );
	}
	
	private function storyPart2() : void
	{
		showWorldText( "..well, sunny if not for you, Little Raincloud.", 5.8 );
		FP.tween( this.camera, { x : 0 }, 6, { complete : storyPart3 } );
		FP.tween( _cloud, { x : FP.screen.width / 2 }, 6 );
	}
	
	private function storyPart3() : void
	{
		_cloud.listenToMouse = true;
		showWorldText( "Look around. Choose wisely. Where will you rain today?", 0 );
	}
	
	public function reportWetness() : void
	{
		//moist
		
		showWorldText( WET_MESSAGES[ _reportedWetCount ], 0 );
		_reportedWetCount += 1;
		
		if ( _reportedWetCount == WET_MESSAGES.length )
		{
			//end the game
			_cloud.listenToMouse = false;
			
			var coverEntity : Entity = new Entity();
			var gameCover : Canvas = new Canvas( FP.screen.width, FP.screen.height );
			gameCover.drawRect( new Rectangle( 0, 0, FP.screen.width, FP.screen.height ), 0x000000 );
			coverEntity.addGraphic( gameCover );
			this.add( coverEntity );
			gameCover.scrollX = 0;
			gameCover.alpha = 0;
			FP.tween( gameCover, { alpha : 1 }, 5 );
			FP.alarm( 5, endGame );
		}
	}
	
	private function endGame() : void
	{
		FP.world = new ResultWorld();
	}
	
	private function createText() : void
	{
		_worldText = new Text( "!" );
		_worldText.size = 28;
		_worldText.color = 0xFFFFFF;
		_worldText.scrollX = 0;
		_worldText.y = FP.screen.height * 0.1;
		
		_textBack = new Canvas( FP.screen.width, FP.screen.height * 0.1 );
		_textBack.fill( new Rectangle( 0, 0, _textBack.width, _textBack.height ), 0x000000, 0.7 );
		_textBack.scrollX = 0;
		_textBack.y = _worldText.y - ( _textBack.height - _worldText.height ) / 2;
		
		this.addGraphic( _textBack );
		this.addGraphic( _worldText );
		
		_moreText = new Text( "More -->" );
		_moreText.color = 0xFFFFFF;
		_moreText.scrollX = 0;
		this.addGraphic( _moreText );
		_moreText.x = FP.screen.width - _moreText.width - 8;
		_moreText.y = FP.screen.height - _moreText.height - 8;
	}
	
	public function showWorldText( a_msg : String, a_time : Number ) : void
	{
		FP.tween( _worldText, { alpha : 1 }, 0.1 );
		_worldText.text = a_msg;
		_worldText.x = ( FP.screen.width - _worldText.width ) / 2;
		
		FP.tween( _textBack, { alpha : 1 }, 0.1 );
		
		if ( a_time > 0 )
		{
			if ( _worldTextAlarm != null )
			{
				_worldTextAlarm.cancel();
			}
			_worldTextAlarm = FP.alarm( a_time, removeWorldText );
		}
	}
	
	private function removeWorldText() : void
	{
		FP.tween( _worldText, { alpha : 0 }, 0.2 );
		FP.tween( _textBack, { alpha : 0 }, 0.2 );
		
		_worldTextAlarm = null;
	}
	
	private function createScene() : void
	{
		_bgEntity = new Entity();
		_bgEntity.layer = 999;
		this.add( _bgEntity );
		
		var bg : Backdrop = new Backdrop( Assets.BG_SCREEN, true, false );
		_bgEntity.addGraphic( bg );
		bg.scrollX = 0.25;
		
		var bg02 : Backdrop = new Backdrop( Assets.BG_LAYER02, true, false );
		_bgEntity.addGraphic( bg02 );
		bg02.scrollX = 0.5;
		
		var bg01 : Backdrop = new Backdrop( Assets.BG_LAYER01, true, false );
		_bgEntity.addGraphic( bg01 );
		bg01.scrollX = 0.95;
	}
	
	private function createAvatar() : void
	{
		_cloud = new CloudEntity();
		this.add( _cloud );
	}
	
	private function createInteractableEntities() : void
	{
		_rightEdgeOfWorld = 0;
		
		function randomize ( a : *, b : * ) : int {
			return ( Math.random() > .5 ) ? 1 : -1;
		}
		var SHUFFLE_COUNT:int = 5;
		for (var i:int = 0; i < SHUFFLE_COUNT; ++i)
		{
			InteractableEntityData.sort(randomize);
		}
		_interactableCollection = new Array();
		
		var placementDistance:Number = 0.0;
		for each(var interactableData:Object in InteractableEntityData)
		{
			placementDistance += MINIMUM_PLACEMENT_DISTANCE + VARIABLE_PLACEMENT_DISTANCE * Math.random();
			
			var newInteractable:InteractableEntity = new InteractableEntity(interactableData);
			newInteractable.x = ENTITY_OFFSET_X + placementDistance;
			newInteractable.y = FP.screen.height * 0.93;
			this.add(newInteractable);
			
			if ( newInteractable.x > _rightEdgeOfWorld )
			{
				_rightEdgeOfWorld = newInteractable.x;
			}
			
			var meter : RainProgressMeter = newInteractable.getMeter();
			meter.x = newInteractable.x;
			meter.y = newInteractable.y + 16;
			meter.visible = false;
			
			var speech : SpeechBubble = newInteractable.getSpeechText();
			if (speech)
			{
				speech.x = newInteractable.x;
				speech.y = newInteractable.y - 150;
			}
		}
		
		_rightEdgeOfWorld -= FP.screen.width * 0.75;
	}
	
	override public function update() : void
	{
		super.update();
		
		updateScrolling();
		
		_moreText.visible = ( this.camera.x < _rightEdgeOfWorld && _cloud.listenToMouse );
	}
	
	private function updateScrolling() : void
	{
		if ( ! _cloud.listenToMouse ) return;
		
		var scrollRate : Number = SCROLL_RATE * FP.elapsed;
		var mouseRelCamera : Number = this.mouseX - this.camera.x;
		if ( mouseRelCamera < SCROLL_BUFFER )
		{
			this.camera.x -= scrollRate;
			if ( this.camera.x < 0 )
			{
				this.camera.x = 0;
			}
		}
		else if ( mouseRelCamera > FP.screen.width - SCROLL_BUFFER )
		{
			this.camera.x += scrollRate;
			if ( this.camera.x > _rightEdgeOfWorld )
			{
				this.camera.x = _rightEdgeOfWorld;
			}
		}
	}
}

}