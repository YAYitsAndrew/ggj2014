package
{

import flash.events.MouseEvent;
import net.flashpunk.FP;
import net.flashpunk.graphics.Text;
import net.flashpunk.World;

public class ResultWorld extends World
{
	public function ResultWorld()
	{
		super();
	}
	
	override public function begin() : void
	{
		super.begin();
		
		var resultsObject:Object = Player.getInstance().getFinalResults();
		/* //test data
		var resultsObject:Object = new Object();
		resultsObject.result = "Test result";
		resultsObject.message = "This is a test message. Why do you eat your own boogers? Tits. Tits. Tits. Tits. Tits. Tits. Tits. Tits. Tits. Tits.";
		resultsObject.criticisms = new Array("Did a thing.", "You also did a thing.", "That one thing happened.", "You did a thing with a thing.", "You ate your own boogers.");
		*/
		
		var cricismHeader: Text = new Text( "What you chose to do:" );
		cricismHeader.size = 40;
		cricismHeader.color = 0xF7FFA8;
		this.addGraphic( cricismHeader );
		cricismHeader.x = ( FP.screen.width - cricismHeader.width ) * 0.5;
		cricismHeader.y = FP.screen.height * 0.05;
		
		var criticismsArray:Array = resultsObject.criticisms;
		var criticismOffset:int = 0;
		for each (var criticism:String in criticismsArray)
		{
			var criticismText : Text = new Text(criticism);
			criticismText.size = 28;
			criticismText.color = 0xF74334;
			this.addGraphic(criticismText);
			criticismText.x = ( FP.screen.width - criticismText.width ) / 2;
			criticismText.y = FP.screen.height * 0.15 + criticismOffset;
			
			criticismOffset += FP.screen.height * 0.05;
		}
		
		
		var resultHeader: Text = new Text( "You are" );
		resultHeader.size = 40;
		resultHeader.color = 0xF7FFA8;
		this.addGraphic( resultHeader );
		resultHeader.x = ( FP.screen.width - resultHeader.width ) * 0.5;
		resultHeader.y = FP.screen.height * 0.5;

		var resultText: Text = new Text( resultsObject.result );
		resultText.size = 92;
		resultText.color = 0xF74334;
		this.addGraphic( resultText );
		resultText.x = ( FP.screen.width - resultText.width ) * 0.5;
		resultText.y = FP.screen.height * 0.55;
		
		var messageText: Text = new Text( "\"" + resultsObject.message + "\"");
		messageText.align = "center";
		messageText.wordWrap = true;
		messageText.width = FP.screen.width * 0.95;
		messageText.size = 28;
		messageText.color = 0xF74334;
		this.addGraphic( messageText );
		messageText.x = ( FP.screen.width - messageText.width ) * 0.5;
		messageText.y = FP.screen.height * 0.75;
		
		var myButt : ButtonEntity = new ButtonEntity( "Play Again" );
		myButt.addEventListener( MouseEvent.CLICK, onRestartClick );
		this.add( myButt );
		myButt.getMeter().visible = false;
		myButt.x = FP.screen.width - ( myButt.width / 2 ) - 8;
		myButt.y = FP.screen.height - 8;
	}
	
	private function onRestartClick( a_event : MouseEvent ) : void
	{
		FP.world = new TitleWorld();
	}
}

}