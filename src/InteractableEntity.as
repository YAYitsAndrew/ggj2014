package
{

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Emitter;
import net.flashpunk.graphics.Image;
import net.flashpunk.graphics.Text;
import net.flashpunk.utils.Ease;
import net.flashpunk.utils.Input;
import net.flashpunk.graphics.Spritemap;

public class InteractableEntity extends Entity
{
	private static const FRAME_RATE:int = 50;
	private static const DRY_ANIMATION:String = "dry"
	private static const WET_ANIMATION:String = "wet"
	private static const WET_DURATION:Number = 1.0; //seconds
	private static const SPEECH_MIN_SHOW_DURATION: Number = 2.0;
	private static const SPEECH_VARIABLE_SHOW_DURATION: Number = 2.0;
	
	protected var _img : Spritemap;
	private var _accumulatedWetness : Number; //moist
	private var _meter : RainProgressMeter;
	private var _speechText : SpeechBubble = null;
	private var _speechHideTimer: Number = Math.random() * SPEECH_MIN_SHOW_DURATION;
	private var _speechShowDuration: Number = SPEECH_MIN_SHOW_DURATION + Math.random() * SPEECH_VARIABLE_SHOW_DURATION;
	private var _isWet:Boolean = false;
	private var _criticismString:String = null;
	private var _personalityPoints:Array = null;
	
	public function InteractableEntity(entityConfig : Object)
	{
		createImage(entityConfig);
		createWetnessMeter();
		createSpeechBubble(entityConfig);
		
		this.layer = 20;
		
		_accumulatedWetness = 0.0;
		
		if (entityConfig.hasOwnProperty("criticismString"))
		{
			_criticismString = entityConfig.criticismString;
		}
		
		if (entityConfig.hasOwnProperty("personalityPoints"))
		{
			_personalityPoints = new Array(
				entityConfig.personalityPoints[0],
				entityConfig.personalityPoints[1],
				entityConfig.personalityPoints[2]);
		}
		
	}
		
	override public function update() : void
	{
		super.update();
		
		checkForShadow();
		checkForRain();
		checkForTextUpdate();
	}
	
	private function checkForShadow() : void
	{
		var castInShadow : Boolean = false;
		var shadow : Entity = this.collide( "shadow", x, y );
		if ( shadow != null )
		{
			var shadowImg : Image = shadow.graphic as Image;
			var shadowLeft : Number = shadow.x + shadowImg.x + 16;
			var shadowRight : Number = shadow.x + shadowImg.x + shadowImg.width - 16;
			
			if ( this.x > shadowLeft && this.x < shadowRight )
			{
				castInShadow = true;
			}
		}
		
		if ( castInShadow )
		{
			_img.color = 0x808080;
		}
		else
		{
			_img.color = 0xFFFFFF;
		}
	}
	
	private function checkForRain() : void
	{
		var rainTracker : Entity = this.collide( "rainTracker", x, y );
		if ( rainTracker != null )
		{
			increaseWetness( FP.elapsed );
		}
		else if( _accumulatedWetness > 0 )
		{
			increaseWetness( - FP.elapsed );
		}
	}

	private function checkForTextUpdate() : void
	{
		if (this.hasSpeechBubble())
		{
			_speechHideTimer += FP.elapsed;
			if (_speechHideTimer >= _speechShowDuration)
			{
				_speechHideTimer = 0.0;
				_speechText.visible = !_speechText.visible;
			}
		}
	}
	
	override public function added() : void
	{
		super.added();
		this.world.add(_meter);
		if (this.hasSpeechBubble())
		{
			this.world.add(_speechText);
		}
	}
	
	private function increaseWetness(durationToAdd:Number) :void
	{
		if (!this.isFullyWet())
		{
			_accumulatedWetness += durationToAdd;
			if ( _accumulatedWetness < 0 )
			{
				_accumulatedWetness = 0;
			}
			
			if (this.isFullyWet())
			{
				onFullyWet();
			}
			
			var t:Number = _accumulatedWetness / WET_DURATION;
			_meter.setProgressNormalized(t);
		}
	}
	
	public function isFullyWet() : Boolean
	{
		return _accumulatedWetness >= WET_DURATION;
	}
	
	public function getMeter() : RainProgressMeter
	{
		return _meter;
	}
	
	public function getSpeechText() : SpeechBubble
	{
		return _speechText;
	}
	
	private function createImage(entityConfig:Object) :void
	{
		_img = new Spritemap(entityConfig.sheetName,
							 entityConfig.frameWidth,
							 entityConfig.frameHeight);
		_img.x = -_img.width / 2;
		_img.y = -_img.height;
		graphic = _img;

		_img.add(DRY_ANIMATION, entityConfig.dryFrames, FRAME_RATE);
		_img.add(WET_ANIMATION, entityConfig.wetFrames, FRAME_RATE);
		
		_img.play(DRY_ANIMATION);
		
		this.setHitboxTo( _img );
	}
	
	private function createWetnessMeter() : void
	{
		_meter = new RainProgressMeter();
	}
	
	protected function onFullyWet() : void
	{
		_img.play(WET_ANIMATION);
		if (this.hasSpeechBubble())
		{
			_speechText.visible = false;
		}
		
		if (_personalityPoints != null)
		{
			for (var i:int = 0; i < 3; ++i)
			{
				var typeToRecord:String = _personalityPoints[i];
				Player.getInstance().addPersonalityPoints(typeToRecord, 3 - i);
			}
			Player.getInstance().addCriticism(_criticismString);
			Player.getInstance().incrementWetCount();
			
			var mw : MainWorld = this.world as MainWorld;
			if ( mw != null )
			{
				mw.reportWetness();
			}
		}
		
		
		_isWet = true;
	}
	
	private function createSpeechBubble(entityConfig:Object) : void
	{
		if (entityConfig.hasOwnProperty("speechString"))
		{
			_speechText = new SpeechBubble(entityConfig.speechString);
			_speechText.visible = false;
		}
	}
	
	private function hasSpeechBubble() : Boolean
	{
		return _speechText != null && !_isWet;
	}
}

}