package
{
	public var ResultsStrings:Object =
	{
		"Boring/Careless":
		{
			"result":"Unsophistocated",
			"message":"You watered the obvious things. You are unoriginal and your actions may hurt people without you realizing it."
		},
		"Boring/Hateful":
		{
			"result":"Cowardly",
			"message":"You're not very interesting and think that just because you're a cloud, you can get away with hurting others."
		},
		"Boring/Overly Analytical":
		{
			"result":"Uninteresting",
			"message":"You are smart but lack curiosity. You played through a game without choosing to have any fun."
		},
		"Boring/Impulsive":
		{
			"result":"Thoughtless",
			"message":"Yeah, you're not a very interesting fellow. Some of the things you do are kinda cool I guess."
		},
		"Careless/Hateful":
		{
			"result":"Menacing",
			"message":"You are a bad bad person. How do you sleep at night."
		},
		"Careless/Overly Analytical":
		{
			"result":"Foolish",
			"message":"You have good intentions but sometimes it might just be better to leave things alone."
		},
		"Careless/Impulsive":
		{
			"result":"Clumsy",
			"message":"You do things without thinking them through. You are a thrill-seeker but you hurt people in the process."
		},
		"Hateful/Overly Analytical":
		{
			"result":"Deceitful",
			"message":"You're pretty smart but you choose to use your talents for evil rather than good. Do you even have friends?"
		},
		"Hateful/Impulsive":
		{
			"result":"Jealous",
			"message":"You are unhappy with yourself. You do things to make others miserable because you have given up on finding happiness."
		},
		"Impulsive/Overly Analytical":
		{
			"result":"Bipolar",
			"message":"Your actions make no sense. You don't even know who you are."
		}
	}
}
