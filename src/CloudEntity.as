package
{

import flash.geom.Point;
import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Emitter;
import net.flashpunk.graphics.Image;
import net.flashpunk.tweens.misc.Alarm;
import net.flashpunk.utils.Ease;
import net.flashpunk.utils.Input;

public class CloudEntity extends Entity
{
	public static const HOVER_DIST : int = 6; //pixels
	public static const RAIN_BUFFER : int = 20; //pixels
	public static const NUM_RAIN_PARTICLES : int = 3; //particles/tick
	public static const POINT_BUFFER_SZ : int = 35 * 2;
	
	private var _img : Image;
	private var _emitter : Emitter;
	private var _shadowEntity : Entity;
	private var _rainEntity : Entity;
	private var _rainPosBuffer : Vector.<int>;
	private var _rainPosReadHead : int;
	private var _rainPosWriteHead : int;
	private var _rainColliderAlarm : Alarm;
	private var _listenToMouse : Boolean;
	
	public function CloudEntity()
	{
		_img = new Image( Assets.CLOUD_01 );
		_img.x = - _img.width / 2;
		super( 0, 0, _img );
		
		this.layer = 10;
		_listenToMouse = true;
	}
	override public function added() : void
	{
		super.added();
		
		_emitter = new Emitter( Assets.RAIN_PARTICLE );
		_emitter.relative = false;
		_emitter.newType( "rain", [ 0 ] );
		_emitter.setMotion( "rain", 270, FP.screen.height * 0.5, 0.75, 5, 50, 0.2 );
		_emitter.setAlpha( "rain", 0.9, 0.7 );
		
		var emitterEntity : Entity = new Entity();
		emitterEntity.addGraphic( _emitter );
		emitterEntity.layer = this.layer + 1;
		this.world.add( emitterEntity );
		
		this.y = FP.screen.height * 0.25;
		tweenUp();
		
		var shadowImg : Image = new Image( Assets.CLOUD_SHADOW01 );
		shadowImg.x = - shadowImg.width / 2;
		shadowImg.y = - shadowImg.height / 2;
		_shadowEntity = new Entity( 0, 0, shadowImg );
		_shadowEntity.type = "shadow";
		_shadowEntity.setHitboxTo( shadowImg );
		_shadowEntity.layer = 998;
		_shadowEntity.x = this.x;
		_shadowEntity.y = FP.screen.height * 0.93;
		this.world.add( _shadowEntity );
		
		_rainEntity = new Entity();
		_rainEntity.type = "rainTracker";
		_rainEntity.setHitboxTo( shadowImg );
		_rainEntity.width -= RAIN_BUFFER * 2;
		_rainEntity.y = _shadowEntity.y;
		this.world.add( _rainEntity );
		
		_rainPosBuffer = new <int>[];
		for ( var i : int = 0; i < POINT_BUFFER_SZ; i++ )
		{
			_rainPosBuffer.push( this.x );
		}
		_rainPosReadHead = 0;
		_rainPosWriteHead = POINT_BUFFER_SZ / 2;
	}
	
	private function tweenUp() : void
	{
		FP.tween( this, { y : this.y - HOVER_DIST }, 0.5, { complete : tweenDown, ease : Ease.sineOut } );
	}
	
	private function tweenDown() : void
	{
		FP.tween( this, { y : this.y + HOVER_DIST }, 0.5, { complete : tweenUp, ease : Ease.sineOut } );
	}
	
	override public function update() : void
	{
		super.update();
		
		followMouse();
		checkForRain();
		updateRainHitbox();
	}
	
	private function followMouse() : void
	{
		if ( ! _listenToMouse )
		{
			_shadowEntity.x = this.x;
			return;
		}
		
		this.x = this.world.mouseX;
		if ( this.x < this.world.camera.x + MainWorld.SCROLL_BUFFER )
		{
			this.x = this.world.camera.x + MainWorld.SCROLL_BUFFER;
		}
		else if ( this.x > this.world.camera.x + ( FP.screen.width - MainWorld.SCROLL_BUFFER ) )
		{
			this.x = this.world.camera.x + FP.screen.width - MainWorld.SCROLL_BUFFER;
		}
		
		_shadowEntity.x = this.x;
	}
	
	private function checkForRain() : void
	{
		if ( ! _listenToMouse ) return;
		
		if ( Input.mouseDown )
		{
			for ( var i : int = 0; i < NUM_RAIN_PARTICLES; i++ )
			{
				emitRainParticle();
			}
		}
	}
	
	private function emitRainParticle() : void
	{
		var randX : Number = this.x - ( _img.width / 2 ) + RAIN_BUFFER
			+ Math.random() * ( _img.width - ( RAIN_BUFFER * 2 ) );
		var emitY : Number = this.y + _img.y + _img.height - 16;
		
		_emitter.emit( "rain", randX, emitY );
	}
	
	private function startRainCollider() : void
	{
		_rainEntity.collidable = true;
		_rainColliderAlarm = null;
	}
	
	private function updateRainHitbox() : void
	{
		if ( Input.mouseUp && ( _rainEntity.collidable || _rainColliderAlarm != null ) )
		{
			_rainEntity.collidable = false;
			
			if ( _rainColliderAlarm != null )
			{
				_rainColliderAlarm.cancel();
				_rainColliderAlarm = null;
			}
		}
		if ( Input.mouseDown && _rainColliderAlarm == null )
		{
			_rainColliderAlarm = FP.alarm( 0.7, startRainCollider );
		}
		
		_rainPosBuffer[ _rainPosWriteHead ] = this.x;
		_rainPosWriteHead += 1;
		if ( _rainPosWriteHead == _rainPosBuffer.length )
		{
			_rainPosWriteHead = 0;
		}
		
		_rainEntity.x = _rainPosBuffer[ _rainPosReadHead ] + RAIN_BUFFER;
		_rainPosReadHead += 1;
		if ( _rainPosReadHead == _rainPosBuffer.length )
		{
			_rainPosReadHead = 0;
		}
	}
	
	public function set listenToMouse( a_val : Boolean ) : void { _listenToMouse = a_val; }
	public function get listenToMouse() : Boolean { return _listenToMouse; }
}

}