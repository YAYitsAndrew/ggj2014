package
{

import flash.display.StageScaleMode;
import net.flashpunk.Engine;
import net.flashpunk.FP;

public class Main extends Engine
{
	public function Main() : void
	{
		super( 960, 640, 60, false );
		
		FP.world = new TitleWorld();
		
		//FP.console.enable();
	}
	
	override public function init() : void
	{
		
	}
	
	override public function setStageProperties():void
	{
		super.setStageProperties();
		stage.scaleMode = StageScaleMode.SHOW_ALL;
	}
}

}