package
{

public final class Player
{
    private static var _instance:Player;
	
	private var _accumlatedPersonalityPoints:Array;
	private var _criticismLog:Array;
	private var _numWet:int;

    public function Player(){
        if(_instance){
            throw new Error("Player... use getInstance()");
        } 
        _instance = this;
		
		initialize();
    }

	public function initialize() : void
	{
		_accumlatedPersonalityPoints = new Array();
		
		addPersonalityEntry(_accumlatedPersonalityPoints, PersonalityType.BORING);
		addPersonalityEntry(_accumlatedPersonalityPoints, PersonalityType.CARELESS);
		addPersonalityEntry(_accumlatedPersonalityPoints, PersonalityType.HATEFUL);
		addPersonalityEntry(_accumlatedPersonalityPoints, PersonalityType.IMPULSIVE);
		addPersonalityEntry(_accumlatedPersonalityPoints, PersonalityType.OVERLYANALYTICAL);
		
		_criticismLog = new Array();
		
		_numWet = 0;
	}
	
    public static function getInstance():Player{
        if(!_instance){
            new Player();
        } 
        return _instance;
    }
	
	public function addPersonalityPoints(typeToAccumulate:String, amount:int) : void
	{
		var entry:Object = null;
		
		for each(var storedEntry:Object in _accumlatedPersonalityPoints)
		{
			if (storedEntry.personalityType == typeToAccumulate)
			{
				entry = storedEntry;
				break;
			}
		}
		
		storedEntry.personalityValue += amount;
	}
	
	public function addCriticism(criticism:String) : void
	{
		_criticismLog.push(criticism);
	}
	
	public function getFinalResults() : Object
	{
		// get two higest personality types
		_accumlatedPersonalityPoints.sortOn('personalityValue', Array.NUMERIC);
		
		var topPersonalityTypes:Array = new Array(_accumlatedPersonalityPoints[4].personalityType,
												  _accumlatedPersonalityPoints[3].personalityType);
												  
		// Arrange the type names in alphabetical order
		topPersonalityTypes.sort();
		
		var resultKey:String = topPersonalityTypes[0] + "/" + topPersonalityTypes[1];
		var result:String = ResultsStrings[resultKey].result;
		var message:String = ResultsStrings[resultKey].message;
		
		var resultsObj:Object = new Object();
		resultsObj.result = result;
		resultsObj.message = message;
		resultsObj.criticisms = _criticismLog;
		
		return resultsObj;
	}
	
	public function incrementWetCount() :void
	{
		++_numWet;
	}
	
	private function addPersonalityEntry(inputArray:Array, entryName:String) :void
	{
		var entry:Object = new Object();
		entry.personalityType = entryName;
		entry.personalityValue = 0;
		
		inputArray.push(entry);
	}
}

}