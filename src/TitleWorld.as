package
{

import net.flashpunk.FP;
import net.flashpunk.graphics.Text;
import net.flashpunk.World;

public class TitleWorld extends World
{
	public function TitleWorld()
	{
		super();
	}
	
	override public function begin() : void
	{
		super.begin();
		
		var title : Text = new Text( "Rain on My Parade" );
		title.size = 42;
		title.color = 0xF7FFA8;
		this.addGraphic( title );
		title.x = ( FP.screen.width - title.width ) / 2;
		title.y = FP.screen.height * 0.1;
		
		var btn : ButtonEntity = new ButtonEntity( "Start" );
		btn.enabled = false;
		this.add( btn );
		btn.x = FP.screen.width / 2;
		btn.y = FP.screen.height * 0.93;
		
		var meter : RainProgressMeter = btn.getMeter();
		meter.x = btn.x;
		meter.y = btn.y + 16;
		
		var instruction : Text = new Text( "Mouse to move.\nClick-hold to rain." );
		this.addGraphic( instruction );
		instruction.x = ( FP.screen.width - instruction.width ) / 2;
		instruction.y = ( FP.screen.height - instruction.height ) / 2;
		
		var instruction2 : Text = new Text( "Rain on the button\nfor a while to start." );
		this.addGraphic( instruction2 );
		instruction2.x = btn.x + ( btn.width / 2 ) + 8;
		instruction2.y = btn.y - btn.height;
		
		var cloud : CloudEntity = new CloudEntity();
		this.add( cloud );
		cloud.y = title.y + title.height + 8;
	}
}

}