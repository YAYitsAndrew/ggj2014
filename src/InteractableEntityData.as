package
{

	
public var InteractableEntityData:Array =
[
	{
		id:"Flower",
		frameWidth:100,
		frameHeight:80,
		sheetName:Assets.INTERACTABLE_FLOWER,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You watered a plant. How original.",
		personalityPoints:[PersonalityType.BORING, PersonalityType.BORING, PersonalityType.OVERLYANALYTICAL]
	},
	{
		id:"Fireguy",
		frameWidth:102,
		frameHeight:241,
		sheetName:Assets.FIRE_GUY,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"So hot!",
		criticismString:"You interrupted a self-immolation ceremony.",
		personalityPoints:[PersonalityType.OVERLYANALYTICAL, PersonalityType.CARELESS, PersonalityType.BORING]
	},
	{
		id:"Wetgirl",
		frameWidth:55,
		frameHeight:166,
		sheetName:Assets.WET_GIRL,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"Its nice outside!",
		criticismString:"You drenched an innocent girl.",
		personalityPoints:[PersonalityType.IMPULSIVE, PersonalityType.CARELESS, PersonalityType.IMPULSIVE]
	},
	{
		id:"Kissing",
		frameWidth:122,
		frameHeight:143,
		sheetName:Assets.KISSING,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"*smooch*",
		criticismString:"You ruined a date.",
		personalityPoints:[PersonalityType.HATEFUL, PersonalityType.IMPULSIVE, PersonalityType.HATEFUL]
	},
	{
		id:"Boombox",
		frameWidth:112,
		frameHeight:206,
		sheetName:Assets.BOOMBOX,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You broke a perfectly good boombox.",
		personalityPoints:[PersonalityType.OVERLYANALYTICAL, PersonalityType.HATEFUL, PersonalityType.CARELESS]
	},
	{
		id:"Car",
		frameWidth:223,
		frameHeight:124,
		sheetName:Assets.CAR,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You washed a car.",
		personalityPoints:[PersonalityType.OVERLYANALYTICAL, PersonalityType.BORING, PersonalityType.BORING]
	},
	{
		id:"Homeless",
		frameWidth:103,
		frameHeight:173,
		sheetName:Assets.HOMELESS,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"Any spare change?",
		criticismString:"You rained on the homeless.",
		personalityPoints:[PersonalityType.HATEFUL, PersonalityType.CARELESS, PersonalityType.IMPULSIVE]
	},
	{
		id:"Thirstyguy",
		frameWidth:189,
		frameHeight:123,
		sheetName:Assets.THIRSTY_GUY,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"Thirsty...",
		criticismString:"You killed a man.",
		personalityPoints:[PersonalityType.CARELESS, PersonalityType.OVERLYANALYTICAL, PersonalityType.OVERLYANALYTICAL]
	},
	{
		id:"Arturo",
		frameWidth:96,
		frameHeight:105,
		sheetName:Assets.ARTURO_CHURRO,
		dryFrames:[0],
		wetFrames:[1],
		speechString:"hhhhhh",
		criticismString:"You ruined Arturo's churro.",
		personalityPoints:[PersonalityType.IMPULSIVE, PersonalityType.BORING, PersonalityType.CARELESS]
	},
	{
		id:"Baby",
		frameWidth:144,
		frameHeight:116,
		sheetName:Assets.BABY,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You killed a baby.",
		personalityPoints:[PersonalityType.CARELESS, PersonalityType.HATEFUL, PersonalityType.CARELESS]
	},
	{
		id:"Sponge",
		frameWidth:97,
		frameHeight:68,
		sheetName:Assets.SPONGE,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You wet a sponge.",
		personalityPoints:[PersonalityType.BORING, PersonalityType.OVERLYANALYTICAL, PersonalityType.BORING]
	},
	{
		id:"CupcakeKid",
		frameWidth:102,
		frameHeight:142,
		sheetName:Assets.CUPCAKE_KID,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You destroyed a kid's cupcake.",
		personalityPoints:[PersonalityType.CARELESS, PersonalityType.HATEFUL, PersonalityType.HATEFUL]
	},
	{
		id:"Parade",
		frameWidth:208,
		frameHeight:201,
		sheetName:Assets.PARADE,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You rained on a parade.",
		personalityPoints:[PersonalityType.IMPULSIVE, PersonalityType.HATEFUL, PersonalityType.HATEFUL]
	},
	{
		id:"Campfire",
		frameWidth:145,
		frameHeight:153,
		sheetName:Assets.CAMPFIRE,
		dryFrames:[0],
		wetFrames:[1],
		criticismString:"You ruined the scout's campfire.",
		personalityPoints:[PersonalityType.HATEFUL, PersonalityType.IMPULSIVE, PersonalityType.HATEFUL]
	}
]
}