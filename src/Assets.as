package
{

public class Assets
{
	[Embed(source = "../assets/arturo-luna.jpg")]
	public static const ARTURO_LUNA : Class;
	
	[Embed(source = "../assets/cloud01.png")]
	public static const CLOUD_01 : Class;
	
	[Embed(source = "../assets/cloudshadow01.png")]
	public static const CLOUD_SHADOW01 : Class;
	
	[Embed(source = "../assets/bg_screen.png")]
	public static const BG_SCREEN : Class;
	
	[Embed(source = "../assets/bg_layer02.png")]
	public static const BG_LAYER02 : Class;
	
	[Embed(source = "../assets/bg_layer01.png")]
	public static const BG_LAYER01 : Class;
	
	[Embed(source = "../assets/rain_particle.png")]
	public static const RAIN_PARTICLE : Class;
	
	[Embed(source = "../assets/flower_sheet.png")]
	public static const INTERACTABLE_FLOWER : Class;
	
	[Embed(source = "../assets/fire_guy.png")]
	public static const FIRE_GUY : Class;
	
	[Embed(source = "../assets/arturo.png")]
	public static const ARTURO_CHURRO : Class;
	
	[Embed(source = "../assets/boombox.png")]
	public static const BOOMBOX : Class;
	
	[Embed(source = "../assets/car.png")]
	public static const CAR : Class;
	
	[Embed(source = "../assets/homeless.png")]
	public static const HOMELESS : Class;
	
	[Embed(source = "../assets/kissing.png")]
	public static const KISSING : Class;
	
	[Embed(source = "../assets/thirstyguy.png")]
	public static const THIRSTY_GUY : Class;
	
	[Embed(source = "../assets/wet_girl.png")]
	public static const WET_GIRL : Class;
	
	[Embed(source = "../assets/cupcake.png")]
	public static const CUPCAKE_KID : Class;
	
	[Embed(source = "../assets/deadbaby.png")]
	public static const BABY : Class;
	
	[Embed(source = "../assets/parade.png")]
	public static const PARADE : Class;
	
	[Embed(source = "../assets/sponge.png")]
	public static const SPONGE : Class;
	
	[Embed(source = "../assets/campfirekid.png")]
	public static const CAMPFIRE : Class;
	
	[Embed(source = "../assets/meter_tick.png")]
	public static const METER_TICK : Class;
	
	[Embed(source = "../assets/meter_backing.png")]
	public static const METER_BACKING : Class;
	
	[Embed(source = "../assets/titlebutton.png")]
	public static const TITLE_BUTTON : Class;
}

}