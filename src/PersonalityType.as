package
{	
public class PersonalityType
{
	public static const BORING:String = "Boring";
	public static const CARELESS:String = "Careless";
	public static const HATEFUL:String = "Hateful";
	public static const OVERLYANALYTICAL:String = "Overly Analytical"
	public static const IMPULSIVE:String = "Impulsive";
}
}