package
{

import net.flashpunk.Entity;
import net.flashpunk.FP;
import net.flashpunk.graphics.Emitter;
import net.flashpunk.graphics.Graphiclist;
import net.flashpunk.graphics.Image;
import net.flashpunk.utils.Ease;
import net.flashpunk.utils.Input;
import net.flashpunk.graphics.Spritemap;

public class RainProgressMeter extends Entity
{	
	static private const BAR_START_OFFSET:Number = 6;
	static private const NUM_BARS:int = 10;
	static private const BAR_PADDING:int = 2;
	
	private var _backing : Image;
	private var _bars:Array;
	private var _meterRenderableCollection : Graphiclist;
	
	public function RainProgressMeter()
	{
		_backing = new Image( Assets.METER_BACKING );
		_backing.x = _backing.width * -0.5;
		_backing.y = _backing.height * -0.5;
		
		_meterRenderableCollection = new Graphiclist();
		
		_meterRenderableCollection.add(_backing);
		
		createBars();
		
		super( 0, 0, _meterRenderableCollection );
	}
	
	public function setProgressNormalized(t:Number) : void
	{
		t = Math.min(1.0, Math.max(0.0, t));
		
		for each (var bar:Image in _bars)
		{
			bar.visible = false;
		}
		
		var maxIndex:int = Math.floor(t * NUM_BARS);
		for (var i:int = 0; i < maxIndex; ++i)
		{
			_bars[i].visible = true;
		}
		
		if (t > 0.0 && t < 1.0)
		{
			this.visible = true;
		}
		else
		{
			this.visible = false;
		}
	}
	
	private function createBars() : void
	{
		var xStart:Number = _backing.width * -0.5 + BAR_START_OFFSET;
		var yStart:Number = 0;
		_bars = new Array();
		for (var i:int = 0; i < NUM_BARS; ++i)
		{
			var newBar:Image = new Image(Assets.METER_TICK);
			var xOffset:Number = i * (newBar.width + BAR_PADDING) + newBar.width * -0.5;
			var yOffset:Number = newBar.height * -0.5;
			newBar.x = xStart + xOffset;
			newBar.y = yStart + yOffset;
			
			newBar.visible = false;
			
			_bars.push(newBar);
			_meterRenderableCollection.add(newBar);
		}
	}
}

}